# Sig•Mal site

| Directory | Description
| :----:    | :----
| [backend](backend/) | Backend in Golang
| [frontend](frontend/) | Frontend in ReactJS written in Typescript

## npm Packages
- react-markdown
- react-router-dom
- react-bootstrap
- @syncfusion/ej2-react-schedule


## Project Requirements
- Containerize all sections of project {backend, frontend, nginx}

## Resources 
- [How To Deploy a React Application with Nginx on Ubuntu 20.04](https://www.digitalocean.com/community/tutorials/how-to-deploy-a-react-application-with-nginx-on-ubuntu-20-04)