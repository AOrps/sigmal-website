package main

import (
	"log"
	"net/http"
	"strconv"
)

const (
	PORT = 5000
)

func index(w http.ResponseWriter, r *http.Request) {

}

func docs(w http.ResponseWriter, r *http.Request) {

}

func res(w http.ResponseWriter, r *http.Request) {

}

func main() {
	port := strconv.Itoa(PORT)
	fs := http.FileServer(http.Dir("."))

	http.Handle("/assets", http.StripPrefix("/assets", fs))

	// Index
	http.HandleFunc("/", index)
	// Dcoumentation

	// Resources

	log.Fatal(http.ListenAndServe(":"+port, nil))
}
