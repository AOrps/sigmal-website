import React, { Component } from "react";
import { Information, ScheduleWeekView } from './components/Info';
import { Row } from 'react-bootstrap';


export default class Home extends Component {
    render() {
        return (
            <div id="home">
                <Row>
                    <div className="objpad-vert">
                    <Information
                        title="About Sig•Mal"
                        body="
                        Sig•Mal is a one-of-a-kind SIG (Special 
                        Interest Group) for NJIT's ACM. Our 
                        purpose is to provide a space for 
                        NJIT students to learn about 
                        different topics in cybersecurity."
                    />
                    </div>
                </Row>
                <Row>
                    <h2>Weekly Schedule</h2>
                    <ScheduleWeekView />
                </Row>
            </div>
        );
    }
}