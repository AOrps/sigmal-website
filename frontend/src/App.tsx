import React from 'react';
import { Container, Row } from 'react-bootstrap';
import './App.css';
import Navigation from './components/Navigation';
import {
  BrowserRouter as Router,
} from 'react-router-dom';

function App() {
  return (
    <Router>
      <Container className="App">
        <Row>
          <Navigation />
        </Row>
      </Container>
    </Router>
  );
}

export default App;