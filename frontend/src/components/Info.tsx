import { Card } from 'react-bootstrap';
import '../App.css';
import {
    Inject, ScheduleComponent,
    Day, Week, WorkWeek, Month, Agenda,
    EventSettingsModel,
    ViewsDirective,
    ViewDirective
} from '@syncfusion/ej2-react-schedule'

interface InformationProps {
    title: string;
    body: string;
}


export const Information = (props: InformationProps) => {
    return (
        <Card className="text-center objmargin">
            <Card.Body>
                <Card.Title>{props.title}</Card.Title>
                <Card.Text>
                    {props.body}
                </Card.Text>
            </Card.Body>
        </Card>
    );
}

export const ScheduleWeekView = () => {
    let localData: EventSettingsModel = {
        dataSource: [{
            Subject: "Sig•Mal Meeting",
            StartTime: new Date(2021, 9, 4, 16, 15),
            EndTime: new Date(2021, 9, 4, 17, 15),
            RecurrenceRule: 'FREQ=WEEKLY;COUNT=6',
            startHour: '9'
            // RecurrenceRule: 'FREQ=WEEKLY;INTERVAL=1;COUNT=5'
        }]
    };

    return (
        <div className="text-center objpad-vert">
            <ScheduleComponent
                selectedDate={new Date(2021, 9, 4)}
                eventSettings={localData}
                currentView='Week'
                readonly={true}
            >
                <ViewsDirective>
                    <ViewDirective
                        option='Week'
                        startHour='13:00'
                        endHour='18:30'
                    />
                </ViewsDirective>
                <Inject services={[Day, Week, WorkWeek, Month, Agenda]} />
            </ScheduleComponent>
        </div>

    );
}

// https://ej2.syncfusion.com/react/documentation/schedule/module-injection/
// Exceptions
export const ScheduleMonthView = () => {
    let localData: EventSettingsModel = {
        dataSource: [{
            Subject: "Sig•Mal Meeting",
            StartTime: new Date(2021, 9, 4, 16, 15),
            EndTime: new Date(2021, 9, 4, 17, 15),
            RecurrenceRule: 'FREQ=WEEKLY;COUNT=6'
        }]
    };

    return (
        <div className="text-center objpad-vert">
            <ScheduleComponent
                selectedDate={new Date(2021, 9, 4)}
                eventSettings={localData}
                currentView='Month'
                readonly={true}
            >
                <Inject services={[Day, Week, WorkWeek, Month, Agenda]} />
            </ScheduleComponent>
        </div>

    );
}