import { Component } from 'react';
import { Navbar, Nav } from 'react-bootstrap';
import { Switch, Route, Link } from 'react-router-dom';
import Logo from '../images/blue-beetle.jpg';
import '../App.css';
import Home from '../Home';
// import Documentation from '../Documentation';
import Resources from '../Resources';
import { ScheduleMonthView } from './Info';

export default class Navigation extends Component {
  render() {
    return (
      <div>
        <div>
          <Navbar bg="light" expand="lg">
            <div className="sigmal-navbar-logo">
              <a href="/">
                <img src={Logo} alt="Sig•Mal Logo" height="75" />
              </a>
            </div>
            <Navbar.Brand href="/"><span className="sigmal-color">Sig•Mal</span></Navbar.Brand>
            <Navbar.Toggle aria-controls="basic-navbar-nav" />
            <Navbar.Collapse id="basic-navbar-nav">
              <Nav className="me-auto">
                <Nav.Link as={Link} to={"/"}>Home</Nav.Link>
                <Nav.Link as={Link} to={"/schedule"}>Schedule</Nav.Link>
                <Nav.Link href="http://github.com/AOrps/SigMal">Repository</Nav.Link>
                {/* <Nav.Link as={Link} to={"/docs"}>Documentation</Nav.Link> */}
                <Nav.Link as={Link} to={"/res"}>Resources</Nav.Link>
              </Nav>
            </Navbar.Collapse>
          </Navbar>
        </div>
        <div>
          <Switch>
            <Route path="/schedule">
              <h1 className="objpad-top">Schedule</h1>
              <ScheduleMonthView />
            </Route>
            {/* <Route path="/docs">
              <Documentation />
            </Route> */}
            <Route path="/res">
              <Resources />
            </Route>
            <Route path="/">
              <Home />
            </Route>
          </Switch>
        </div>
      </div>
    );
  }
}